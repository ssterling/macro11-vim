To-Do
=====

* Rid of number highlighting in the wrong context
* Add `^F` floating-point directive
* Add `^R` Radix-50 directive
* Change default number highlighting accordingly between `.RADIX` directives
* Fix highlighting issues with `/` in comments
* Add `.REM` directive
* Highlight numbers when used within index and index deferred addressing
