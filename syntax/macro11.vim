" Vim syntax file
" Language: MACRO-11
" Maintainer: Seth Price
" Latest Revision: 10 July 2021

" Quit if syntax file is already loaded
if exists("b:current_syntax")
	finish
endif

" XXX Comment out this line if you really want that uppercase look
syn case ignore

set tabstop=8
set softtabstop=0
set shiftwidth=0

" In short: any printable character but a comma, colon, semicolon or space
set iskeyword=@,33-43,45-57,60-126

" Anything that doesn’t match below is probably a user-defined symbol
syn match m11UserSymbol "\v<(\@\#|[\@\#])[A-Za-z0-9\.]+\$?>" contains=m11Qual
" But don't highlight it
" (this is mostly just for proper highlighting of ‘m11Qual’

" Special PC instruction
syn match m11PcDot "\v\."
hi def link m11PcDot Identifier

" Constants
syn match m11OctConst "\v<(\@\#|[\@\#])?(\^O)?-?[0-7]+>" contains=m11OctConstDel
syn match m11BinConst "\v<(\@\#|[\@\#])?\^B?[01]+>" contains=m11BinConstDel,m11Qual,m11OnesComplementDel
syn match m11DecConst "\v<(\@\#|[\@\#])?\^D-?[0-9]+>" contains=m11DecConstDel,m11Qual,m11OnesComplementDel
syn match m11DecConstDot "\v<(\@\#|[\@\#])?-?[0-9]+\.>" contains=m11DecConstDelDot,m11Qual,m11OnesComplementDel
syn match m11CharConst "\v<(\@\#|[\@\#])?\'.>" contains=m11CharConstDel,m11Qual,m11OnesComplementDel,m11InvalidChar
syn match m11DcharConst "\v<(\@\#|[\@\#])?\".{2}>" contains=m11DcharConstDel,m11Qual,m11OnesComplementDel,m11InvalidChar
syn match m11OctConstDel "\v\^O" contained
syn match m11BinConstDel "\v\^B" contained
syn match m11DecConstDel "\v\^D" contained
syn match m11DecConstDelDot "\v\." contained
syn match m11CharConstDel "\v\'" contained
syn match m11DCharConstDel "\v\"" contained
syn match m11OnesComplementDel "\v\^C" contained
hi def link m11OctConst Number
hi def link m11BinConst Number
hi def link m11BinConstDel Special
hi def link m11DecConst Number
hi def link m11DecConstDot Number
hi def link m11DecConstDel Special
hi def link m11DecConstDelDot Special
hi def link m11CharConst Number
hi def link m11CharConstDel Special
hi def link m11DcharConst Number
hi def link m11DcharConstDel Special
hi def link m11OnesComplementDel Special

" Reference/constant qualifiers
syn match m11Qual "\v[\@\#]" contained containedin=m11UserSymbol,m11OctConst,m11DecConst,m11DecConstDot,m11CharConst,m11DCharConst
hi def link m11Qual Operator

" Registers
" Bare register
syn match m11Register "\v<R[0-5]>" contains=m11RegisterAct
syn match m11Register "\v<\%[0-7]>" contains=m11RegisterAct
syn match m11Register "\v<SP>" contains=m11RegisterAct
syn match m11Register "\v<PC>" contains=m11RegisterAct
" Register deferred
syn match m11Register "\v<\(R[0-5]\)>" contains=m11RegisterAct
syn match m11Register "\v<\(\%[0-7]\)>" contains=m11RegisterAct
syn match m11Register "\v<\(SP\)>" contains=m11RegisterAct
syn match m11Register "\v<\(PC\)>" contains=m11RegisterAct
" Autoincrement
syn match m11Register "\v<\(R[0-5]\)\+>" contains=m11RegisterAct
syn match m11Register "\v<\(\%[0-7]\)\+>" contains=m11RegisterAct
syn match m11Register "\v<\(SP\)\+>" contains=m11RegisterAct
syn match m11Register "\v<\(PC\)\+>" contains=m11RegisterAct
" Autoincrement deferred
syn match m11Register "\v<\@\(R[0-5]\)\+>" contains=m11RegisterAct
syn match m11Register "\v<\@\(\%[0-7]\)\+>" contains=m11RegisterAct
syn match m11Register "\v<\@\(SP\)\+>" contains=m11RegisterAct
syn match m11Register "\v<\@\(PC\)\+>" contains=m11RegisterAct
" Autodecrement
syn match m11Register "\v<-\(R[0-5]\)>" contains=m11RegisterAct
syn match m11Register "\v<-\(\%[0-7]\)>" contains=m11RegisterAct
syn match m11Register "\v<-\(SP\)>" contains=m11RegisterAct
syn match m11Register "\v<-\(PC\)>" contains=m11RegisterAct
" Autodecrement deferred
syn match m11Register "\v<\@-\(R[0-5]\)>" contains=m11RegisterAct
syn match m11Register "\v<\@-\(\%[0-7]\)>" contains=m11RegisterAct
syn match m11Register "\v<\@-\(SP\)>" contains=m11RegisterAct
syn match m11Register "\v<\@-\(PC\)>" contains=m11RegisterAct
" Index
syn match m11Register "\v<\S+\(R[0-5]\)>" contains=m11RegisterAct
syn match m11Register "\v<\S+\(\%[0-7]\)>" contains=m11RegisterAct
syn match m11Register "\v<\S+\(SP\)>" contains=m11RegisterAct
syn match m11Register "\v<\S+\(PC\)>" contains=m11RegisterAct
" Index deferred
syn match m11Register "\v<\@\S+\(R[0-5]\)>" contains=m11RegisterAct
syn match m11Register "\v<\@\S+\(\%[0-7]\)>" contains=m11RegisterAct
syn match m11Register "\v<\@\S+\(SP\)>" contains=m11RegisterAct
syn match m11Register "\v<\@\S+\(PC\)>" contains=m11RegisterAct
" For highlighting registers within the addressing mode syntases
syn match m11RegisterAct "\vR[0-5]" contained containedin=m11Register
syn match m11RegisterAct "\v\%[0-7]" contained containedin=m11Register
syn match m11RegisterAct "\vSP" contained containedin=m11Register
syn match m11RegisterAct "\vPC" contained containedin=m11Register

" Highlight all the above register stuff
hi def link m11RegisterAct Identifier
hi def link m11Register Operator

" Processor priority symbols
syn match m11ProcPrior "\v<PR[0-7]>"
hi def link m11ProcPrior Statement

" ASCII strings
" Source: MACRO-11 Language Reference Manual (AA-V027A-TC)
syn region m11String start="\v\/" end="\v\/" contains=m11StringEsc,m11InvalidChar
syn match m11StringEsc "\v\\[nte0rap\\\>]" contained
hi def link m11String String
hi def link m11StringEsc Special

" Pseudo-operands
" Source: MACRO-11 Language Reference ,m11InvalidCharManual (AA-V027A-TC)
syn keyword m11PseudoOp .ASCII .ASCIZ .ASECT
syn keyword m11PseudoOp .BLKB .BLKW .BYTE
syn keyword m11PseudoOp .CROSS .CSECT
syn keyword m11PseudoOp .DSABL
syn keyword m11PseudoOp .ENABL .ENDC .ENDM .ENDR .ERROR .EVEN
syn keyword m11PseudoOp .FLT2 .FLT4
syn keyword m11PseudoOp .GLOBL
syn keyword m11PseudoOp .IDENT .IF .IFF .IFT .IFTF .IIF .INCLUDE .IRP .IRPC
syn keyword m11PseudoOp .LIBRARY .LIMIT .LIST
syn keyword m11PseudoOp .MACRO .MCALL .MDELETE .MEXIT
syn keyword m11PseudoOp .NARG .NCHR .NLIST .NOCROSS .NTYPE
syn keyword m11PseudoOp .ODD
syn keyword m11PseudoOp .PACKED .PAGE .PRINT .PSECT
syn keyword m11PseudoOp .RADIX .RAD50 .REM .REPT .RESTORE .SAVE .SBTTL
syn keyword m11PseudoOp .TITLE
syn keyword m11PseudoOp .WEAK .WORD
hi def link m11PseudoOp PreProc

" General opcodes
" Source: MACRO-11 Language Reference Manual (AA-V027A-TC)
syn keyword m11Opcode ADC ADCB ADD ASH ASHC ASL ASLB ASR ASRB
syn keyword m11Opcode BCC BCS BEQ BGE BGT BHI BHIS BIC BICB BIS BISB BIT BITB
syn keyword m11Opcode BLE BLO BLOS BLT BMI BNE BPL BPT BR BVC BVS
syn keyword m11Opcode CALL CALLR CCC CLC CLN CLR CLRB CLV CLZ CMP CMPB COM COMB
syn keyword m11Opcode DEC DECB DIV
syn keyword m11Opcode EMT
syn keyword m11Opcode FADD FDIV FMUL FSUB
syn keyword m11Opcode HALT
syn keyword m11Opcode INC INCB IOT
syn keyword m11Opcode JMP JSR
syn keyword m11Opcode MARK MOV MOVB MTPI MUL NEG NEGB NOP
syn keyword m11Opcode RESET RETURN ROL ROLB ROR RORB RTI RTS RTT
syn keyword m11Opcode SBC SBCB SCC SEC SEN SEV SEZ SOB SUB SWAB SXT
syn keyword m11Opcode TRAP TSTB TSTSET
syn keyword m11Opcode WAIT WRTLCK
syn keyword m11Opcode XOR

" Commercial Instruction Set opcodes
" Source: MACRO-11 Language Reference Manual (AA-V027A-TC)
syn keyword m11Opcode ADDN ADDNI ADDP ADDPI ASHN ASHNI ASHP ASHPI
syn keyword m11Opcode CMPC CMPCI CMPN CMPNI CMPP CMPPI CVTLN CVTLNI CVTLP
syn keyword m11Opcode CVTLPI CVTNP CVTNPI CVTPN CVTPNI
syn keyword m11Opcode DIVP DIVPI
syn keyword m11Opcode LOCC LOCCI L2DN L3DN
syn keyword m11Opcode MATC MATCI MOVC MOVCI MOVRC MOVRCI MOVTC MULP MULPI
syn keyword m11Opcode SCANC SCANCI SKPC SKPCI SPANC SPANCI SUBN SUBNI SUBP
syn keyword m11Opcode SUBPI

" Floating-point instruction set opcodes
" Source: MACRO-11 Language Reference Manual (AA-V027A-TC)
syn keyword m11Opcode ABSD ABSF ADDD ADDF
syn keyword m11Opcode CFCC CLRD CLRF CMPD CMPF DIVD DIVF
syn keyword m11Opcode LDCDF LDCFD LDCID LDCIF LDCLD LDCLF LDD LDEXP LDF LDFPS
syn keyword m11Opcode MFPD MOFF MODF MTPD MULD MULF
syn keyword m11Opcode NEGD NEGF
syn keyword m11Opcode SETD SETF SETI SETL SPL STA0 STB0 STCDF STCDI STCDL STCFD
syn keyword m11Opcode STCFI STCFL STD STEXP STF STFPS STST SUBD SUBF
syn keyword m11Opcode TSTD TSTF

" *NOT* on the PDP-11/04
" Source: PDP-11 Processor Handbook (1979)
syn keyword m11Opcode MFPI

" PDP 11/34A only
" Source: PDP-11 Processor Handbook (1979)
syn keyword m11Opcode MFPS MTPD MTPS

" PDP 11/44 only
" Source: PDP-11 Processor Handbook (1979)
syn keyword m11Opcode CSM
syn keyword m11Opcode MFPT

" PDP 11/60 only
" Source: MACRO-11 Language Reference Manual (AA-V027A-TC)
syn keyword m11Opcode MED6X
" Source: PDP-11 Processor Handbook (1979)
syn keyword m11Opcode LDUB
syn keyword m11Opcode MED MNS MPP
syn keyword m11Opcode XFC

" PDP 11/74 with CIS only
" Source: MACRO-11 Language Reference Manual (AA-V027A-TC)
syn keyword m11Opcode MED74C

" LSI-11, LSI-11/23 or LSI-11/2 only
" Source: MACRO-11 Language Reference Manual (AA-V027A-TC)
syn keyword m11Opcode MFPS MTPS

" Highlight all that
hi def link m11Opcode Statement

" RT-11 System Macro Library macros
" Source: RT-11 System Macro Library Manual (A-PD6LA-TC)
syn keyword m11SysLibMacro .ABTIO .ADDR .ASSUME
syn keyword m11SysLibMacro .BR
syn keyword m11SysLibMacro .CALLK .CALLS .CDFN .CHAIN .CK0 .CK1 .CK2 .CK3 .CK4
syn keyword m11SysLibMacro .CK5 .CK6 .CK7 .CLOSE .CLOSZ .CMKT .CSIGEN .CSISPC
syn keyword m11SysLibMacro .CSTAT .CTIMIO
syn keyword m11SysLibMacro .DATE .DEBUG .DELETE .DEVICE .DPRINT .DRAST .DRBEG
syn keyword m11SysLibMacro .DRBOT .DRDEF .DREND .DREST .DRFIN .DRINS .DRPTR
syn keyword m11SysLibMacro .DRSET .DRSPF .DRTAB .DRUSE .DRVTB .DSTAT
syn keyword m11SysLibMacro .ENTER .EXIT
syn keyword m11SysLibMacro .FETCH .FORK .FPROT
syn keyword m11SysLibMacro .GFDAT .GFINF .GFSTA .GTIM .GTJB .GTLIN .GVAL
syn keyword m11SysLibMacro .HERR .HRESET
syn keyword m11SysLibMacro .INTEN
syn keyword m11SysLibMacro .LOCK .LOOKUP
syn keyword m11SysLibMacro .MACS .MFPS .MRKT .MTATCH .MTDTCH .MTGET .MTIN
syn keyword m11SysLibMacro .MTOUT .MTPRNT .MTPS .MTRCTO .MTSET .MTSTAT
syn keyword m11SysLibMacro .PEEK .POKE .PRINT .PROTECT .PURGE .PVAL
syn keyword m11SysLibMacro .QELDF .QSET
syn keyword m11SysLibMacro .RCTRLO .READ .READC .READQ .RELEASE .RENAME .REOPEN
syn keyword m11SysLibMacro .RSUM
syn keyword m11SysLibMacro .SAVESTATUS .SCCA .SDTTM .SERR .SETTOP .SFDAT .SFINF
syn keyword m11SysLibMacro .SFPA .SFSTA .SPCPS .SPFUN .SPND .SRESET .SYNCH
syn keyword m11SysLibMacro .TIMIO .TLOCK .TRPSET .TTINR .TTYIN .TTYOUT .TTOUTR
syn keyword m11SysLibMacro .TWAIT
syn keyword m11SysLibMacro .UNLOCK .UNPROTECT
syn keyword m11SysLibMacro ..V1 ..V2
syn keyword m11SysLibMacro .WAIT .WRITC .WRITE .WRITW

" RT-11 System Macro Library macros, multijob/mapped environments only
" Source: RT-11 System Macro Library Manual (A-PD6LA-TC)
syn keyword m11SysLibMacro .CHCOPY .CMAP .CNTXSW .CRAW .CRRG
syn keyword m11SysLibMacro .ELAW .ELRG
syn keyword m11SysLibMacro .GCMAP .GMCX
syn keyword m11SysLibMacro .MAP .MSDS .MWAIT
syn keyword m11SysLibMacro .RCVD .RVCDC .RCVDW .RDBBK .RDBDF
syn keyword m11SysLibMacro .SDAT .SDATC .SDATW
syn keyword m11SysLibMacro .UNMAP
syn keyword m11SysLibMacro .WDBBK .WDBDF

" Highlight the above
hi def link m11SysLibMacro Macro

" Labels
syn match m11Label "\v^\s*[A-Z0-9\.\$]*::"he=e-1 " global needs its own entry??
syn match m11Label "\v^\s*[A-Z0-9\.\$]*:"he=e-1
hi def link m11Label Label

" Comments
setlocal comments=";"
syn match m11Comment "\v;.*$" contains=@Spell,m11Todo,m11InvalidChar
syn match m11Todo "\v(TODO|FIXME|XXX|NOTE)" contained
hi def link m11Comment Comment
hi def link m11Todo Todo

" Lastly, anything that isn’t valid seven-bit ASCII
syn match m11InvalidChar "\v[^\x00-\x7F]"
hi def link m11InvalidChar Error
