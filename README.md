DEC MACRO-11 Syntax Highligting
===============================

A rudimentary Vim syntax highlighting scheme for the
[MACRO-11](https://en.wikipedia.org/wiki/MACRO-11)
assembly language for the PDP-11.

Copy the contents of the directories in this repository to `$VIMHOME`.
Filetype detection is not yet available; you’ll likely have to use a
[modeline](http://vimdoc.sourceforge.net/htmldoc/options.html#modeline).

Forward any bugs or suggestions to the
[issue tracker](https://gitlab.com/ssterling/macro11-vim/issues).
